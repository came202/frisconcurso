//questions
/* set up XMLHttpRequest */
var url = "../cuestionario.xlsx";
var oReq = new XMLHttpRequest();
var page = 3;

var jsonStruct;
var preguntas;
var questions;

function makeVariables(){
    oReq.open("GET", url, true);
    oReq.responseType = "arraybuffer";

    oReq.onload = function(e){
        var arraybuffer = oReq.response;
        /* convert data to binary string */
        var data = new Uint8Array(arraybuffer);
        var arr = new Array();
        for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
        var bstr = arr.join("");
        /* Call XLSX */
        var workbook = XLSX.read(bstr, {type:"binary"});
        /* DO SOMETHING WITH workbook HERE */
        var first_sheet_name = workbook.SheetNames[page];
        /* Get worksheet */
        var worksheet = workbook.Sheets[first_sheet_name];
        jsonStruct = makeJson_excel(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
        questions = {
            "questions": jsonStruct.questions,
            "max_time": jsonStruct.max_time
        }
    }
    oReq.send();
}

function makeJson_excel(element){
    var json = {
            "questions": [],
            "max_time": ""
        }
    if(typeof(element) == 'object'){
        for(var i=0;i<element.length;i++){
            if(i == 0){
                json.max_time = element[i]["Duracion de Pregunta"];
            }
            json.questions.push(element[i]["Preguntas"]);
        }
        return json;
    }
    return json;
}

function refreshQuestions() {
    if (questions.questions.length <= 0) {
        makeVariables();
    }
}

// variables
var buttons_sequence = 5;
var speed = 800;
var current_sequence = [];
var user_sequence = [];
var playing = false;
var winner = false;

// timers
var timer_make_sequence;
var timer_clear_error;
var timer_clear_event;
var timer_chronometer;

$(document).ready(inicia);

function inicia() {
    makeVariables();
    $("img").mousedown(function () {
        return false;
    });
}

// function to mix array
function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

// function to validate the sequence
function validate_sequence(sequence) {
    for (var i = 0; i < sequence.length; i++) {
        if (current_sequence[i] !== sequence[i]) {
            return false;
        }
    }
    return true;
}

// function to restart the game
function game_reset() {
    clearInterval(timer_make_sequence);
    clearTimeout(timer_clear_event);
    clearTimeout(timer_clear_error);
    current_sequence = [];
    user_sequence = [];
    playing = false;
    img_off();
    $(".memoria-contenedor-elementos .memoria-elemento .memoria_error").hide();
    $("#modal_memoria").removeClass("modal_alert");
}

// function to make array
function make_array(max) {
    var array = [];
    for (var i = 1; i <= max; i++) {
        array.push(i);
    }
    return array;
}

// function to put img off
function img_off() {
    for (var i = 1; i <= buttons_sequence; i++) {
        $(".memoria-contenedor-elementos .memoria-elemento .memoria-img-el-" + i).attr("src", "img/memoria_00" + i + "-off.png");
    }
}

// function to put img on
function img_on() {
    for (var i = 1; i <= buttons_sequence; i++) {
        $(".memoria-contenedor-elementos .memoria-elemento .memoria-img-el-" + i).attr("src", "img/memoria_00" + i + ".png");
    }
}

// chronometer
function chronometer() {
    clearInterval(timer_chronometer);
    var seconds = questions.max_time;
    var k_seconds = 0;
    var text_k_seconds = 0;
    $("#modal_memoria .modal_chronometer").css('display', 'inline-flex');

    timer_chronometer = setInterval(function () {
        $("#modal_memoria .seconds").text(seconds);
        text_k_seconds = k_seconds < 10 ? "0" + String(k_seconds) : k_seconds;
        $("#modal_memoria .k_seconds").text(text_k_seconds);
        if (k_seconds == 0) {
            if (seconds == 0) {
                $("#modal_memoria").addClass("modal_alert");
                clearInterval(timer_chronometer);
            }
            k_seconds = 99;
            seconds--;
        }
        k_seconds--;
    }, 10);
}

//restart modal
function restart_modal() {
    $("#modal_memoria .modal_chronometer").hide();
    $("#modal_memoria").removeClass("modal_alert");
}

// function to make the sequence
function create_sequence() {
    game_reset();
    current_sequence = shuffle(make_array(buttons_sequence));
    var aux_cont = 0;
    timer_make_sequence = setInterval(function () {
        $(".memoria-contenedor-elementos .memoria-elemento .memoria-img-el-" + current_sequence[aux_cont]).attr("src", "img/memoria_00" + current_sequence[aux_cont] + ".png");
        if (aux_cont < current_sequence.length) {
            aux_cont++;
        } else {
            playing = true;
            img_off();
            clearInterval(timer_make_sequence);
        }
    }, speed);
}

// function to repeat the sequence
function repeat_sequence() {
    if (current_sequence.length === buttons_sequence && playing) {
        playing = false;
        user_sequence = [];
        var aux_cont = 0;
        timer_make_sequence = setInterval(function () {
            $(".memoria-contenedor-elementos .memoria-elemento .memoria-img-el-" + current_sequence[aux_cont]).attr("src", "img/memoria_00" + current_sequence[aux_cont] + ".png");
            if (aux_cont < current_sequence.length) {
                aux_cont++;
            } else {
                playing = true;
                img_off();
                clearInterval(timer_make_sequence);
            }
        }, speed);
    }
}

// function to listen the event in the buttons
$(".memoria-contenedor-elementos .memoria-elemento input[name = 'memoria-radio']").click(function (element) {
    if (playing) {
        var element_val = $(element.target).val();
        var image = $(element.target).parent().children("img");
        var image_error = $(element.target).parent().children("div");

        $(image).attr("src", "img/memoria_00" + element_val + ".png");

        user_sequence.push(parseInt(element_val));

        if (validate_sequence(user_sequence)) {
            if (user_sequence.length === current_sequence.length) {
                chronometer();
                refreshQuestions();
                $("#modal_memoria .message").text(questions.questions[0]);
                questions.questions.shift();
                $("#modal_memoria").fadeIn(300);
                game_reset();
                img_on();
                playing = false;
                winner = true;
            }
        } else {
            image_error.show(100);
            // timer_clear_error = setTimeout(function () {
            //     $("#modal_memoria .message").text("Perdiste!!");
            //     $("#modal_memoria").fadeIn(300);
            //     game_reset();
            //     img_on();
            // }, 300);
            playing = false;
            winner = false;
        }
    }
});

$(".play-btn").click(repeat_sequence);
$(".recargar-btn").click(create_sequence);


$("#modal_memoria").hide();
$(".button_modal").click(function () {
    $("#modal_memoria").fadeOut(300);
    clearInterval(timer_chronometer);
    if (winner === false) {
        create_sequence();
    }
});