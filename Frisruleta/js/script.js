//questions
/* set up XMLHttpRequest */
var url = "../cuestionario.xlsx";
var oReq = new XMLHttpRequest();
var page = 1;

var jsonStruct;
var questions;
var preguntas_cultura;
var preguntas_producto;
var preguntas_modelo_de_atencion;

function makeVariables(var_refresh){
    oReq.open("GET", url, true);
    oReq.responseType = "arraybuffer";

    oReq.onload = function(e){
        var arraybuffer = oReq.response;
        /* convert data to binary string */
        var data = new Uint8Array(arraybuffer);
        var arr = new Array();
        for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
        var bstr = arr.join("");
        /* Call XLSX */
        var workbook = XLSX.read(bstr, {type:"binary"});
        /* DO SOMETHING WITH workbook HERE */
        var first_sheet_name = workbook.SheetNames[page];
        /* Get worksheet */
        var worksheet = workbook.Sheets[first_sheet_name];
        jsonStruct = makeJson_excel(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
        preguntas_cultura = jsonStruct.cultura;
        preguntas_producto = jsonStruct.producto;
        preguntas_modelo_de_atencion = jsonStruct.modelo_de_atencion;
        vector_ranges = generate_ranges();
        if(var_refresh == "cultura")
            questions.cultura = preguntas_cultura;
        else if(var_refresh == "producto")
            questions.producto = preguntas_producto;
        else if(var_refresh == "modelo_de_atencion")
            questions.modelo_de_atencion = preguntas_modelo_de_atencion;
        else
            questions = {
                "cultura": preguntas_cultura,
                "producto": preguntas_producto,
                "modelo_de_atencion": preguntas_modelo_de_atencion,
                "max_time": jsonStruct.max_time
            }
    }
    oReq.send();
}

function makeJson_excel(element){
    var json = {
            "cultura": [],
            "producto": [],
            "modelo_de_atencion": [],
            "max_time": ""
        }
    if(typeof(element) == 'object'){
        for(var i=0;i<element.length;i++){
            if(i == 0){
                json.max_time = element[i]["Duracion de Pregunta"];
            }
            json.cultura.push(element[i]["Preguntas Cultura"]);
            json.producto.push(element[i]["Preguntas Producto"]);
            json.modelo_de_atencion.push(element[i]["Preguntas Modelo de Atencion"]);
        }
        return json;
    }
    return json;
}

function refreshQuestions() {
    if (questions.cultura.length <= 0) {
        makeVariables("cultura");
    }
    if (questions.producto.length <= 0) {
        makeVariables("producto");
    }
    if (questions.modelo_de_atencion.length <= 0) {
        makeVariables("modelo_de_atencion");
    }
}

//variables
var vector_ranges = [];
var roulette_position = 0;
var max_steps = 5;
var steps = 360 / max_steps;

//timers
var timer_rotate_roulette;
var timer_chronometer;

$(document).ready(inicia);

function inicia() {
    $("img").mousedown(function () {
        return false;
    });
}

// generate ranges to decision
function generate_ranges() {
    var array = [];
    for (var i = 0; i < max_steps; i++) {
        var aux = [];
        aux.push(steps * i);
        aux.push(steps * (i + 1));
        array.push(aux);
    }
    return array;
}

// function to validate the step
function validate_step() {
    for (var i = 0; i < max_steps; i++) {
        if (roulette_position >= vector_ranges[i][0] && roulette_position <= vector_ranges[i][1]) {
            return i;
        }
    }
    return false;
}

// function to restart
function restart_game() {
    clearTimeout(timer_rotate_roulette);
}

// chronometer
function chronometer(){
    clearInterval(timer_chronometer);
    var seconds=questions.max_time;
    var k_seconds=0;
    var text_k_seconds=0;

    $("#modal_memoria .modal_chronometer").css('display', 'inline-flex');

    timer_chronometer = setInterval(function () {
        $("#modal_memoria .seconds").text(seconds);
        text_k_seconds = k_seconds < 10 ? "0" + String(k_seconds) : k_seconds;
        $("#modal_memoria .k_seconds").text(text_k_seconds);
        if (k_seconds == 0) {
            if (seconds == 0) {
                $("#modal_memoria").addClass("modal_alert");
                clearInterval(timer_chronometer);
            }
            k_seconds = 99;
            seconds--;
        }
        k_seconds--;
    }, 10);
}

//restart modal
function restart_modal() {
    $("#modal_memoria .modal_chronometer").hide();
    $("#modal_memoria").removeClass("modal_alert");
}

// make modal
function make_modal(val) {
    restart_modal();
    switch (val) {
        case 0:
            if (questions.cultura.length >= 0) {
                refreshQuestions();
                $("#modal_memoria .message").text(questions.cultura[0]);
                setTimeout(
                    function () {
                        $("#modal_memoria").fadeIn(300);
                        questions.cultura.shift();
                        chronometer();
                    }, 500
                );

            }
            break;
        case 1:
            $("#modal_memoria .message").text("¡Inténtalo de nuevo!");
            setTimeout(
                function () {
                    $("#modal_memoria").fadeIn(300);
                }, 500
            );
            break;
        case 2:
            if (questions.producto.length >= 0) {
                refreshQuestions();
                $("#modal_memoria .message").text(questions.producto[0]);
                setTimeout(
                    function () {
                        $("#modal_memoria").fadeIn(300);
                        questions.producto.shift();
                        chronometer();
                    }, 500
                );
            }
            break;
        case 3:
            $("#modal_memoria .message").text("¡Cede el turno!");
            setTimeout(
                function () {
                    $("#modal_memoria").fadeIn(300);
                }, 500
            );
            break;
        case 4:
            if (questions.modelo_de_atencion.length >= 0) {
                refreshQuestions();
                $("#modal_memoria .message").text(questions.modelo_de_atencion[0]);
                setTimeout(
                    function () {
                        $("#modal_memoria").fadeIn(300);
                        questions.modelo_de_atencion.shift();
                        chronometer();
                    }, 500
                );
            }
            break;
        default:
            break;
    }
}

// function to rotate the roulette
function rotate_roulette() {
    restart_game();
    var twists = Math.floor(Math.random() * (301) + 100);
    var increase = 1;
    var step_increase = 0.5;
    var speed = 16;
    timer_rotate_roulette = setInterval(function () {
        roulette_position += Math.floor(increase);
        if (roulette_position > 360) {
            roulette_position -= 360;
        }
        $(".ruleta .ruleta-contenedor .ruleta-contenedor-elementos .circle_game").css("transform", "rotate(" + roulette_position + "deg)");
        if (increase >= Math.floor(twists / 4)) {
            step_increase = -step_increase;
        }
        if (increase <= 0) {
            make_modal(validate_step())
            clearInterval(timer_rotate_roulette);
        }
        increase += step_increase;
    }, speed);
    return roulette_position;
}

// init
$("#modal_memoria").hide();
makeVariables();

// listen events
$(".play-btn").click(function () {
    rotate_roulette();
});

$(".button_modal").click(function () {
    $("#modal_memoria").fadeOut(300);
    clearInterval(timer_chronometer);
});