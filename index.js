const {app, BrowserWindow} = require('electron');

app.on('ready', () => {
    let mainWindow = new BrowserWindow();
    mainWindow.maximize();
    mainWindow.loadURL(`file://${__dirname}/index.html`);
});