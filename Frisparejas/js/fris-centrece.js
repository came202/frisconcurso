//questions
/* set up XMLHttpRequest */
var url = "../cuestionario.xlsx";
var oReq = new XMLHttpRequest();
var page = 2;

var jsonStruct;
var preguntas;
var questions;

function makeVariables(){
    oReq.open("GET", url, true);
    oReq.responseType = "arraybuffer";

    oReq.onload = function(e){
        var arraybuffer = oReq.response;
        /* convert data to binary string */
        var data = new Uint8Array(arraybuffer);
        var arr = new Array();
        for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
        var bstr = arr.join("");
        /* Call XLSX */
        var workbook = XLSX.read(bstr, {type:"binary"});
        /* DO SOMETHING WITH workbook HERE */
        var first_sheet_name = workbook.SheetNames[page];
        /* Get worksheet */
        var worksheet = workbook.Sheets[first_sheet_name];
        jsonStruct = makeJson_excel(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
        questions = {
            "questions": jsonStruct.questions,
            "max_time": jsonStruct.max_time
        }
    }
    oReq.send();
}

function makeJson_excel(element){
    var json = {
            "questions": [],
            "max_time": ""
        }
    if(typeof(element) == 'object'){
        for(var i=0;i<element.length;i++){
            if(i == 0){
                json.max_time = element[i]["Duracion de Pregunta"];
            }
            json.questions.push(element[i]["Preguntas"]);
        }
        return json;
    }
    return json;
}

function refreshQuestions() {
    if (questions.questions.length <= 0) {
        makeVariables();
    }
}

var timer_chronometer;

$(document).ready(inicia);

function inicia() {
    $("img").mousedown(function () {
        return false;
    });
}

// objects
function question_card(question) {
    this.type_card = "question";
    this.question = question || "";
    this.source = "tarjeta_pregunta.png";
}

function food_card(food) {
    this.type_card = "food";
    this.food = food;
    this.state = false;
    this.source = food + ".png";
}

function point_card(point) {
    this.type_card = "point";
    this.val = point;
    this.source = "tarjeta_puntos.png";
}

function pass_card() {
    this.type_card = "pass";
    this.source = "tarjeta_ceda.png";
}

// global variables

var foods;
var points;
var questions;
var board;
var logic_board;
var num_food;
var num_point;
var num_pass;
var num_questions;

var window_card;
var pair_card;
var delay;


// Function to mixing array
function shuffle(a, count, num_iter) {
    count = count || 0;
    num_iter = num_iter || (count + 1);
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    if (count >= num_iter)
        return a;
    else
        return shuffle(a, count + 1, num_iter);

}

// Function to make ordered array
function make_ordered_array(finish) {
    var array = []
    for (var i = 0; i < finish; i++) {
        array.push(i);
    }
    return array;
}


// Function to make object struct board
function make_array_board() {
    var array = [];

    for (var i = 0; i < num_food; i++) {
        //var aux= Math.floor((Math.random() * foods.length) + 0)
        //asigna las parejas de comidas
        array.push(new food_card(foods[i]));
        array.push(new food_card(foods[i]));
    }
    for (var i = 0; i < num_point; i++) {
        array.push(new point_card(points[Math.floor((Math.random() * points.length) + 0)]));
    }
    for (var i = 0; i < num_pass; i++) {
        array.push(new pass_card());
    }

    var aux_pos_questions = shuffle(make_ordered_array(num_questions), 0, 5)
    for (var i = 0; i < num_questions; i++) {
        array.push(new question_card(questions[aux_pos_questions[i]]));
    }
    return array;
}

// Function for conver to matrix make_array_board
function make_logic_board() {
    var array = shuffle(make_array_board(), 0, 5)
    var aux_logic_board = []
    var count = 0;
    for (var i = 0; i < board; i++) {
        var aux_row = [];
        for (var j = 0; j < board; j++) {
            aux_row.push(array[count])
            count++;
        }
        aux_logic_board.push(aux_row);
    }
    return aux_logic_board;
}

// Function to make board
function make_board() {
    $("#game_board").empty();

    for (var i = 0; i < board; i++) {

        $("#game_board").append('<div class="row_board" id="row_board_' + i + '">');
        for (var j = 0; j < board; j++) {

            var card = logic_board[i][j]

            $("#row_board_" + i).append('<div class="card_board" id="card_board_' + i + '_' + j + '">');
            $("#card_board_" + i + "_" + j).append('<input type="checkbox" id="check_board_' + i + '_' + j + '">');

            var html_card = '<label class="content_card" for="check_board_' + i + '_' + j + '">' +
                '<div class="face_card">' +
                '<img src="./img/tarjeta_cubierta.png" alt="img"></div>' +
                '<div class="face_card">' +
                '<img src="./img/' + card.source + '" alt="img_2"></div></label>';

            $("#card_board_" + i + "_" + j).append(html_card);
        }
    }
}

// Function to the logic game
function check_card(i, j) {
    var card = logic_board[i][j];
    // compai if the last card is equal to card current
    if (window_card != "") {
        if (card.type_card != "food") {
            $("#check_board_" + window_card[0] + "_" + window_card[1]).prop('disabled', false);
            $("#check_board_" + window_card[0] + "_" + window_card[1]).prop('checked', false);
            window_card = "";
        }
    }

    if (card.type_card == "point") {
        // open modal
        //$("#modal_memoria .message").text("¡¡ Estupendo !! ahora tienes +"+card.val+" puntos.");
        //$("#modal_memoria").modal({backdrop: 'static', keyboard: false,show: true});
        // open and block the point_card
        $("#card_board_" + i + "_" + j + " > label").addClass("open_card");
        $("#check_board_" + i + "_" + j).remove();
    } else if (card.type_card == "pass") {
        // open modal
        //$("#modal_memoria .message").text("¡¡ Pierdes tu turno !!");
        //$("#modal_memoria").modal({backdrop: 'static', keyboard: false,show: true});
        // open and block the pass_card
        $("#card_board_" + i + "_" + j + " > label").addClass("open_card");
        $("#check_board_" + i + "_" + j).remove();
    } else if (card.type_card == "question") {
        // open and block the question_card
        $("#card_board_" + i + "_" + j + " > label").addClass("open_card");
        $("#check_board_" + i + "_" + j).remove();
        // open modal
        $("#modal_memoria .message").html(card.question);
    } else if (card.type_card == "food") {
        if (window_card == "") {
            window_card = [i, j];
            $("#check_board_" + window_card[0] + "_" + window_card[1]).prop('disabled', true);
        } else if (pair_card == "") {
            pair_card = [i, j];
            $("#check_board_" + pair_card[0] + "_" + pair_card[1]).prop('disabled', true);
        }

        if (window_card != "" && pair_card != "") {
            if (logic_board[window_card[0]][window_card[1]].food == logic_board[pair_card[0]][pair_card[1]].food) {
                // open and block the last-food_card and current-food_card
                $("#card_board_" + window_card[0] + "_" + window_card[1] + " > label").addClass("open_card");
                $("#card_board_" + pair_card[0] + "_" + pair_card[1] + " > label").addClass("open_card");
                $("#check_board_" + window_card[0] + "_" + window_card[1]).remove();
                $("#check_board_" + pair_card[0] + "_" + pair_card[1]).remove();
                refreshQuestions();
                $("#modal_memoria .message").text(questions.questions[0]);
                questions.questions.shift();
                setTimeout(function () {
                    $("#modal_memoria").fadeIn(300);
                    chronometer();
                }, delay);
            } else {
                var temp_1 = window_card;
                var temp_2 = pair_card;
                // restart the last-food_card and current-food_card
                setTimeout(function () {
                    $("#check_board_" + temp_1[0] + "_" + temp_1[1]).prop('disabled', false);
                    $("#check_board_" + temp_1[0] + "_" + temp_1[1]).prop('checked', false);
                    $("#check_board_" + temp_2[0] + "_" + temp_2[1]).prop('disabled', false);
                    $("#check_board_" + temp_2[0] + "_" + temp_2[1]).prop('checked', false);
                }, delay);
            }
            window_card = "";
            pair_card = "";
        }
    }
}

// Function to listen event in the board
function game() {
    $("#game_board .card_board input[type='checkbox']").change(function (element) {
        var check_element = $(element.target).parent(".card_board").attr("id").split("_");

        var i = check_element[check_element.length - 2];
        var j = check_element[check_element.length - 1];

        check_card(i, j);
    });
}

//start the game
function init() {

    // all foods

    //////////////////////////////////////////////////////////////////////////////////
    ////////////// THE NAME FOODS MUST BE EQUIALS TO THE NAME IMAGES /////////////////
    //////////////////////////////////////////////////////////////////////////////////

    foods = ["parejas-001", "parejas-002", "parejas-003", "parejas-004", "parejas-005", "parejas-006", "parejas-007", "parejas-008"];
    // points to win
    points = [5];
    // length board
    board = 4;

    //Tiempo de espera
    delay = 1000;
    // count card for the board
    num_food = 8; // numero de parejas
    num_point = 0;
    num_pass = 0;
    num_questions = (board * board) - num_point - (num_food * 2) - num_pass;

    // var controls
    window_card = "";
    pair_card = "";
    logic_board = make_logic_board();

    make_board();
    game();

    //escucha el evento de enter para cerrar el modal
    $(document).on('keydown', function (e) {
        if (e.keyCode == 27 || e.keyCode == 13) {
            $('#btn-close-modal').click();
        }
    });
}

// $("#welcome").hide().fadeIn(300);
$(".recargar-btn").click(function () {
    init();
});

makeVariables();
init();

// chronometer
function chronometer() {
    clearInterval(timer_chronometer);
    var seconds = questions.max_time;
    var k_seconds = 0;
    var text_k_seconds = 0;
    $("#modal_memoria .modal_chronometer").css('display', 'inline-flex');

    timer_chronometer = setInterval(function () {
        $("#modal_memoria .seconds").text(seconds);
        text_k_seconds = k_seconds < 10 ? "0" + String(k_seconds) : k_seconds;
        $("#modal_memoria .k_seconds").text(text_k_seconds);
        if (k_seconds == 0) {
            if (seconds == 0) {
                $("#modal_memoria").addClass("modal_alert");
                clearInterval(timer_chronometer);
            }
            k_seconds = 99;
            seconds--;
        }
        k_seconds--;
    }, 10);
}

//restart modal
function restart_modal() {
    $("#modal_memoria .modal_chronometer").hide();
    $("#modal_memoria").removeClass("modal_alert");
}

$("#modal_memoria").hide();
$(".button_modal").click(function () {
    $("#modal_memoria").fadeOut(300);
    clearInterval(timer_chronometer);
});