/* set up XMLHttpRequest */
var url = "../cuestionario.xlsx";
var oReq = new XMLHttpRequest();
var page = 0;

var personas = 999;
var total_elements;
var selecteds;
var count_trys;

function makeVariables(){
    oReq.open("GET", url, true);
    oReq.responseType = "arraybuffer";

    oReq.onload = function(e){
        var arraybuffer = oReq.response;
        /* convert data to binary string */
        var data = new Uint8Array(arraybuffer);
        var arr = new Array();
        for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
        var bstr = arr.join("");
        /* Call XLSX */
        var workbook = XLSX.read(bstr, {type:"binary"});
        /* DO SOMETHING WITH workbook HERE */
        var first_sheet_name = workbook.SheetNames[page];
        /* Get worksheet */
        var worksheet = workbook.Sheets[first_sheet_name];

        personas = String(XLSX.utils.sheet_to_json(worksheet,{raw:true})[0]["N Personas"]);
        total_elements = localStorage.getItem("total_elements") != null ? localStorage.getItem("total_elements") : personas;
        selecteds = localStorage.getItem("selecteds") != null ? mapArray(localStorage.getItem("selecteds"),String(total_elements).length) : [];
        count_trys = localStorage.getItem("count_trys") != null ? localStorage.getItem("count_trys") : 0;
        createSlots(total_elements, selecteds);
    }
    oReq.send();
}

// personas = 999;
// total_elements = personas;
// selecteds = [];
// count_trys = 0;
// createSlots(total_elements, selecteds);

var trys = 5;

var hitMe = $('#hitMe');

$(document).ready(inicia);

function inicia() {
    makeVariables();
    $("img").mousedown(function () {
        return false;
    });
}

function mapArray(array,slices){
    var array = array.split(",");
    var maped_array = [];
    for(var i=0; i< Math.floor(array.length/slices);i++){
        var array_aux = [];
        for(var j=0; j< slices;j++){
            array_aux.push(array[(i*slices)+j]);
        }
        maped_array.push(array_aux);
    }
    return maped_array;
}

function getRandomInt(min, max) {
    max = parseInt(max);
    min = parseInt(min);
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

function stringToArray(string){
    var array = [];
    for(var i=0; i<string.length;i++)
        array.push(string[i]);
    return array;
}

function stringToArrayNum(string){
    var array = [];
    for(var i=0; i<string.length;i++)
        if(string[i]== " ")
            array.push(0);
        else
            array.push(parseInt(string[i]));
    return array;
}

function removeZeros(array){
    if(typeof array == 'string')
        array= stringToArray(array);
    var sum = 0;
    for(var i=0;i<array.length;i++){
        sum+= parseInt(array[i]);
        if(sum == 0)
            array[i] = " ";
        else
            return array.join('');
    }
    return array.join('');
}

function valZeros(array){
    for(var i=0; i<array.length;i++){
        if(array[i]!=0)
            return false;
    }
    return true;
}

function arrayFind(element,elements){
    var counter;
    if(Array.isArray(element) && Array.isArray(elements) && element.length > 0){
        length = element.length;
        for(var i=0;i<elements.length;i++){
            counter = 0;
            for(var j=0; j<length;j++){
                if(typeof(elements[j]) == 'number'){
                    if(element[j] == elements[j]){
                        counter++;
                    }
                }
                else{
                    if(element[j] == elements[i][j]){
                        counter++;
                    }
                }
            }
            if(counter == element.length)
                return true;
        }
    }
    return false;
}

function arrayFindIndex(element,elements){
    var counter;
    if(Array.isArray(element) && Array.isArray(elements) && element.length > 0){
        length = element.length;
        for(var i=0;i<elements.length;i++){
            counter = 0;
            for(var j=0; j<length;j++){
                if(typeof(elements[j]) == 'number'){
                    if(element[j] == elements[j]){
                        counter++;
                    }
                }
                else{
                    if(element[j] == elements[i][j]){
                        counter++;
                    }
                }
            }
            if(counter == element.length)
                return i;
        }
    }
    return -1;
}

function getNumsFromRange(max_elements){
    max_elements = String(max_elements);
    var array=[];
    var number = String(getRandomInt(1,max_elements));
    var range = max_elements.length - number.length;
    for(var i=0;i < max_elements.length;i++){
        if(i >= range){
            array.push(parseInt(number[i-range]));
        }else{
            array.push(0);
        }
    }
    if(valZeros(array) || arrayFind(array,selecteds)){
        return getNumsFromRange(max_elements);
    }
    else{
        selecteds.push(array);
        arrayFind(array,selecteds);
        return array;
    }
    return 80;
};

function restarSlots(){
    $(".sorteo-contenedor-elementos .content_phils").empty();
    $(".sorteo-contenedor-elementos .slots").empty();
    createSlots(total_elements);
}

function remove_element(element){
    count_trys--;
    index_element= arrayFindIndex(stringToArrayNum($(element).prev().text()),selecteds);
    selecteds.splice(index_element,1);
    localStorage.setItem("count_trys", count_trys);
    localStorage.setItem("selecteds", selecteds);
    $(element).parent().remove();
}

function createSlots(max_elements,elements){
    if(typeof(elements) !== 'undefined' && elements.length > 0){
        var last_element = elements[elements.length-1];
        for(var i=0;i< last_element.length;i++){
            $(".sorteo-contenedor-elementos .slots")
                .append("<span>"+last_element[i]+"</span>");
        }
        for(var i=0;i< elements.length;i++){
            var aux_val_phil="";
            for(var j=0;j<elements[i].length;j++){
                aux_val_phil+= String(elements[i][j]);
            }
            $(".sorteo-contenedor-elementos .content_phils").append("<label><span>"+removeZeros(aux_val_phil)+"</span><button class='remove_element'>X</button></label>");
        }
    }
    else{
        for(var i=0;i< String(max_elements).length;i++){
            $(".sorteo-contenedor-elementos .slots").append("<span>-</span>");
        }
    }
}

hitMe.click(function (e) {
    if(count_trys < trys){
        var slots = $('#slots span');
        var nums = getNumsFromRange(total_elements);

        [].forEach.call(slots, function (elm, inx) {
            setTimeout(function () {
                elm.classList.toggle('spin');
                if (Array.isArray(nums)) {
                    setTimeout(function () {
                        elm.innerHTML = nums[inx];
                    }, 335);
                }
            }, inx * 100);
        });
        var aux_val_phil = "";
        for(var i=0; i< nums.length;i++){
            aux_val_phil+= String(nums[i]);
        }
        setTimeout(function(){            
            $(".sorteo-contenedor-elementos .content_phils").append("<label><span>"+removeZeros(aux_val_phil)+"</span><button class='remove_element'>X</button></label>");
        },(slots.length*100)+500);
        count_trys++;
        localStorage.setItem("count_trys", count_trys);
        localStorage.setItem("selecteds", selecteds);
        localStorage.setItem("total_elements", total_elements);
    }
    if(count_trys >= trys){
    }
});

$(".sorteo-contenedor-elementos .content_phils").click(function(element){
    element.preventDefault();
    if($(element.target).attr("class") == "remove_element"){
        remove_element($(element.target));
    }
});

$(".recargar-btn").click(function(element){
    total_elements = personas;
    selecteds = [];
    count_trys = 0;
    localStorage.setItem("count_trys", count_trys);
    localStorage.setItem("selecteds", selecteds);
    localStorage.setItem("total_elements", total_elements);
    restarSlots();
});